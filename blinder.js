/*
 * @file blinder.js
 * Blinder JS
 * Using Drupal 7 Javascript API
 */

(function ($) {
	Drupal.behaviors.blinder = {
	    attach: function (context, settings) {
					if ($("#blinder").length) {
						var fadeSpeed = Drupal.settings.blinder.duration || 500;
						$(window).load(function() {
							fadeSpeed = parseInt(fadeSpeed);
							$("#blinder").fadeOut(fadeSpeed);
						})
					}
			}
  }
})(jQuery);
