<?php
/**
 * @file blinder.admin.inc
 * Blinder Administration
 */
?>
<?php
function blinder_settings_form() {
	$form['blinder_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enabled'),
		'#description' => t('Turn on and off the blinder.'),
		'#default_value' => variable_get('blinder_enabled',FALSE),
	);
	$form['blinder_bgcolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Background Color'),
		'#description' => t('Enter a Hex RGB value (without the # symbol).  If left blank, defaults to white (FFFFFF)'),
		'#default_value' => variable_get('blinder_bgcolor',''),
		'#size' => 6,
	);
	$form['blinder_fade_duration'] = array(
		'#type' => 'textfield',
		'#title' => t('Fade duration'),
		'#description' => t('Time in milliseconds (ms) to fade out the blinder on page-load.  If left blank, defaults to @fade.',array('@fade' => BLINDER_DEFAULT_FADE_DURATION . 'ms')),
		'#default_value' => variable_get('blinder_fade_duration',''),
		'#size' => 4,
	);
	$form['blinder_image'] = array(
		'#type' => 'managed_file',
		'#title' => t('Indicator Image'),
		'#description' => t('A small gif or png, usually some sort of spinner, that indicates to the user something is loading.  Will sit center in the blinder if utilized.'),
		'#default_value' => variable_get('blinder_image',''),
		'#upload_location' => 'public://',
		'#upload_validators' => array(
			'file_validate_extensions' => array('png gif'),
			'file_validate_image_resolution' => array('256x256', '6x6'),
		),
	);
	$themes = list_themes();
	//$options = array('none' => '-none-');
	$options = array();
	foreach($themes as $theme_name => $theme) {
		$options[$theme_name] = $theme_name;
	}
	$form['blinder_theme_visibility'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Theme Visibility'),
		'#description' => t('Choose the theme the blinder should appear on.'),
		'#options' => $options,
		'#default_value' => variable_get('blinder_theme_visibility',''),
		'#multiple' => TRUE,
	);
	$form['blinder_debug'] = array(
		'#type' => 'checkbox',
		'#title' => t('Debug'),
		'#description' => t('Display verbose debugging information for the blinder module'),
		'#default_value' => variable_get('blinder_debug',FALSE),
	);
  // Add extra submit (necessary since using system_settings_form handler for the form)
  $form['#submit'][] = 'blinder_settings_form_submit';
	return system_settings_form($form);
}

function blinder_settings_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['blinder_image'])) {
    // Save file permanently
     $file = file_load($form_state['values']['blinder_image']);
     // Change status to permanent.
     $file->status = FILE_STATUS_PERMANENT;
     file_save($file);
     // Record that the module (in this example, user module) is using the file. 
     file_usage_add($file, 'blinder', 'settings', 0); 
  }
}